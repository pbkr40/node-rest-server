import NeDBDataStore = require("nedb");
import express = require("express");
import fs = require("fs")
import bodyParser = require("body-parser");
import {HTMLUtils} from "./HTMLUtils";

let config = require("../config.json");

let app = express();

let database = new NeDBDataStore({filename: "./files/store.db", autoload: true});

// Enable CORS: https://en.wikipedia.org/wiki/Cross-origin_resource_sharing
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(bodyParser.json());

app.get("/users", (req, res) => {
    database.find("", (error, users) => {
        if (!error) {
            let result = (users) ? JSON.stringify(users) : [];

            res.setHeader("Content-Type", "application/json");
            res.end(result)
        } else {
            console.log(`Could not read the entities!: DB-Error-Message: '${error.message}'`);
            res.status(500);
            res.end("Could not read the entities!")
        }
    })
});

app.get("/users/:id", (req, res) => {
    let userId = req.params.id;
    database.findOne({_id: userId}, (error, users) => {
        if (!error) {
            if (users) {
                let result = JSON.stringify(users);

                res.setHeader("Content-Type", "application/json");
                res.end(result)
            } else {
                res.status(404);
                res.end()
            }
        } else {
            console.log(`Could not read the entity!: DB-Error-Message: '${error.message}'`);
            res.status(500);
            res.end("Could not read the entity!")
        }
    })
});

app.put("/users/:id", (req, res) => {
    let userId = req.params.id;
    let user = req.body;

    user._id = userId;

    try {
        database.update({_id: userId}, user, (error) => {
            if (!error) {
                res.status(204);
                res.end()
            } else {
                console.log(`Could not create the entity!: DB-Error-Message: '${error.message}'`);
                res.status(500);
                res.end("Could not create the entity!")
            }
        })
    } catch (ex) {
        console.log(`Body contains non valid json! Body: '${ex.message}' : body: '${user}'`);
        res.status(400);
        res.send("Your body contains non valid json!")
    }
});

app.post("/users", (req, res) => {
    let user = req.body;

    try {
        if (Object.keys(user).length === 0) {
            console.log("Received an empty objekt");
            res.status(400);
            res.end("Your object was empty - maybe an wrong or missing Content-Length");
        } else {
            if (user.name && user.fname) {
                let randomNumber = Math.round(Math.random() * 100);
                user._id = user.name.substring(0, 2) + user.fname.substring(0, 2) + randomNumber
            }

            database.insert(user, (error, insertedUser: any) => {
                if (!error) {
                    res.header("Content-Location", `/users/${insertedUser._id}`);
                    res.header("Content-Type", "application/json");
                    res.status(201);
                    res.end(JSON.stringify(insertedUser))
                } else {
                    console.error(error.stack);
                    console.log(`Could not create the entity!: DB-Error-Message: '${error.message}'`);
                    res.status(500);
                    res.end("Could not create the entity! Reason: " + error.message);
                }
            })
        }

    } catch (ex) {
        console.log(`Body contains non valid json! Body: '${ex.message}' : body: '${user}'`);
        res.status(400);
        res.send("I require json data: maybe your json is invalid or is not specified as json")
    }

});

app.delete("/users/:id", (req, res) => {
    let userId = req.params.id;

    try {
        database.remove({ _id: userId }, {}, function (error) {
            if (!error) {
                res.status(204);
                res.end()
            } else {
                console.log(`Could not delete the entity!: DB-Error-Message: '${error.message}'`);
                res.status(500);
                res.end("Could not delete the entity!")
            }
        });
    } catch (ex) {
        res.status(500);
        res.send("Server-Error: " + ex.message)
    }
});

// The 404 handler matches all request => keep it always as the last route

let table = HTMLUtils.table(
    ["Method",  "Path",         "Accept",           "Serve",                "Success",      "Description"],
    ["GET",     "/users",       "-",                "application/json",     "200",          "list of all users"],
    ["GET",     "/users/:id",   "-",                "application/json",     "200",          "user with id"],
    ["POST",    "/users",       "application/json", "application/json",     "201",          "create an user"],
    ["PUT",     "/users/:id",   "application/json", "-",                    "204",          "update an user with id"],
    ["DELETE",  "/users/:id",   "-",                "-",                    "204",          "delete an user"]
);
app.get("*", (req, res) => {
    res.status(404).send(HTMLUtils.html5Shell("Not Found", `
        <h1>404 Not Found</h1>
        <p>Nothing found under '${req.originalUrl}'</p>
        <p>Try one of the following uri paths</p>
        ${table}
`));
});

let server = app.listen(config.port, () => {
    let host = server.address().address;
    let port = server.address().port;

    console.log(`Listen at http://${host}:${port}`)
});
