/**
 * Contains template functions for html 5.
 *
 * @author Andrej Sajenko
 */
export class HTMLUtils {

    /**
     * Create a standard html 5 shell with chatset utf-8.
     *
     * @param title Used as html title
     * @param bodyContent Used as content of the body
     * @param headerContent Used as content of the head
     *
     * @returns {string} The html 5 document
     */
    public static html5Shell(title: string, bodyContent: string, headerContent: string = ""): string {
        return `
        <!DOCTYPE html>
        <html>
            <head>
                <meta charset="UTF-8">
                <title>${title}</title>
                ${headerContent}
            </head>
            <body>
                ${bodyContent}
            </body>
        </html>
        `;
    }

    /**
     * Create a html table out of the array values.
     * The first array values will be taken as header values.
     *
     * @param rows take the two-dim array of string values which represent the table.
     *
     * @returns {string} A html table
     */
    public static table(... rows: string[][]) {
        let tHead = HTMLUtils.tableHeader(rows.shift());

        let tBody = rows.map((e) => HTMLUtils.tableRow(e)).join("");

        return `
            <table>
                <thead>${tHead}</thead>
                <tbody>${tBody}</tbody>
            </table>
         `;
    }

    /**
     * Create a table header row out of a array of string values.
     * &lt;tr&gt;
     *      &lt;th&gt;Value1&lt;/th&gt;
     *      &lt;th&gt;Value2&lt;/th&gt;
     * &lt;/tr&gt;
     *
     * @param elements To use as values
     *
     * @returns {string} The html header row
     */
    public static tableHeader(elements: string[]): string {
        let tdWrapped = elements.map((e) => `<th>${e}</th>`);
        let joinedToString = tdWrapped.join("");

        return `<tr>${joinedToString}</tr>`;
    }

    /**
     * Create a table row out of a array of string values.
     * &lt;tr&gt;
     *      &lt;td&gt;Value1&lt;/td&gt;
     *      &lt;td&gt;Value2&lt;/td&gt;
     * &lt;/tr&gt;
     *
     * @param elements To use as values
     *
     * @returns {string} The html table row
     */
    public static tableRow(elements: string[]): string {
        let tdWrapped = elements.map((e) => `<td>${e}</td>`);
        let joinedToString = tdWrapped.join("");

        return `<tr>${joinedToString}</tr>`;
    }
}